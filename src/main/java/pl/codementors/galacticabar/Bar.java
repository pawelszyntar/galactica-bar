package pl.codementors.galacticabar;

import java.util.Scanner;

public class Bar {

    public static void main(String[] args) {

        Barman barman = new Barman();

        Client client1 = new Client(barman, "Wojciech");
        Client client2 = new Client(barman, "Ferdynand");
        Client client3 = new Client(barman, "Alex");
        Client client4 = new Client(barman, "Malgorzata");
        Client client5 = new Client(barman, "Kasia");

        Thread thread1 = new Thread(client1);
        Thread thread2 = new Thread(client2);
        Thread thread3 = new Thread(client3);
        Thread thread4 = new Thread(client4);
        Thread thread5 = new Thread(client5);
        Thread bar = new Thread(barman);

        bar.start();
        thread1.start();
        thread2.start();
        thread3.start();
        thread4.start();
        thread5.start();

        Scanner scanner = new Scanner(System.in);
        scanner.next();
        System.out.println("quit");

        barman.stop();
        client1.stop();
        client2.stop();
        client3.stop();
        client4.stop();
        client5.stop();

        bar.interrupt();
        thread1.interrupt();
        thread2.interrupt();
        thread3.interrupt();
        thread4.interrupt();
        thread5.interrupt();
    }

}
