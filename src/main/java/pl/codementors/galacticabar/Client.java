package pl.codementors.galacticabar;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Client implements Runnable {

    private Barman barman;
    private String clientName;

    private static final Logger log = Logger.getLogger(Client.class.getCanonicalName());

    private boolean running = true;

    public void stop() {
        running = false;
    }

    public Client(Barman barman, String clientName) {
        this.barman = barman;
        this.clientName = clientName;
    }

    @Override
    public void run() {
        while (running) {
            try {
                String drink = barman.takeDrink();
                System.out.println(clientName + " is drinking " + drink);
                Thread.sleep(drink.length() * 1000);
            } catch (InterruptedException ex) {
                log.log(Level.WARNING, ex.getMessage(), ex);
            }
        }
    }
}
