package pl.codementors.galacticabar;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Barman implements Runnable {

    private String drinkName = null;
    private boolean running = true;

    private String[] drinkMenu = {"mojito", "whisky on the rocks", "vodka on ice", "rum", "jack daniels"};

    private static final Logger log = Logger.getLogger(Barman.class.getCanonicalName());

    public void stop() {
        running = false;
    }

    public synchronized String takeDrink() throws InterruptedException {
        while (drinkName == null) {
            this.wait();
        }
        String ret = drinkName;
        drinkName = null;
        return ret;
    }

    @Override
    public void run() {
        int i = 0;
        while (running) {
            synchronized (this) {
                if (drinkName == null) {
                    drinkName = drinkMenu[i];
                    if (i < drinkMenu.length - 1) {
                        i++;
                    } else {
                        i = 0;
                    }
                    notifyAll();
                }
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                log.log(Level.WARNING, ex.getMessage(), ex);
            }
        }
    }
}